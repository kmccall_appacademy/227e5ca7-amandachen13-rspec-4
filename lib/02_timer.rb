class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    s = seconds % 60
    m = (seconds - s) / 60 % 60
    h = seconds / 3600
    padded(h) + ':' + padded(m) + ':' + padded(s)
  end

  def padded(num)
    if num < 10
      return "0" + num.to_s
    else
      return num.to_s
    end
  end
end
