class Temperature
  # TODO: your code goes here!
  def initialize(choice)
    if choice.has_key?(:f)
      @temp = choice[:f]
      @scale = "fahrenheit"
    else
      @temp = choice[:c]
      @scale = "celsius"
    end
  end

  def in_fahrenheit
    return @temp if @scale == "fahrenheit"
    (@temp * 9.0 / 5.0) + 32
  end

  def in_celsius
    return @temp if @scale == "celsius"
    (@temp - 32) * 5.0 / 9.0
  end

  def self.from_fahrenheit(temp)
    self.new({f: temp})
  end

  def self.from_celsius(temp)
    self.new({c: temp})
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp = temp
    @scale = "celsius"
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp = temp
    @scale = "fahrenheit"
  end
end
