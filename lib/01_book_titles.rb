class Book
  # TODO: your code goes here!
  # attr_reader :title
  def title
    @title
  end

  def title=(title)
    lowercase_words = ["a", "an", "the", "in", "of", "and"]
    title_words = title.split.map(&:downcase)
    correct_title = title_words.map.with_index do |word, idx|
      if lowercase_words.include?(word) && idx != 0
        word
      else
        word.capitalize
      end
    end
    @title = correct_title.join(' ')
  end
end
