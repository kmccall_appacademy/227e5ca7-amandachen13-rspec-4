class Dictionary
  # TODO: your code goes here!
  def initialize
    @dictionary = {}
  end

  def entries
    @dictionary
  end

  def add(entry)
    if entry.class == String
      @dictionary[entry] = nil
    else
      @dictionary = @dictionary.merge(entry)
    end
  end

  def include?(word)
    @dictionary.keys.include?(word)
  end

  def keywords
    @dictionary.keys.sort
  end

  def find(word)
    @dictionary.select {|k, v| k.include?(word)}
  end

  def printable
    output = []
    self.keywords.each do |k|
      output << "[#{k}] \"#{@dictionary[k]}\""
    end
    output.join("\n")
  end
end
